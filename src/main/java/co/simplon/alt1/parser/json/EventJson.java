package co.simplon.alt1.parser.json;

public class EventJson {
    public Integer id;
    public String text;
    public ChoiceJson[] choices;
}
