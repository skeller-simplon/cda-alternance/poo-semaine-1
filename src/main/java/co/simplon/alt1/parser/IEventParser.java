package co.simplon.alt1.parser;

import co.simplon.alt1.data.Event;

public interface IEventParser {
    Event parse(String input);
}
