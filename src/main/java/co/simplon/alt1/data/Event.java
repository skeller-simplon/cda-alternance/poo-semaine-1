package co.simplon.alt1.data;

import java.util.ArrayList;
import java.util.List;

public class Event {
    private String text;
    private List<Choice> choices = new ArrayList<>();

    public Event(String text, List<Choice> choices) {
        this.setText(text);
        this.choices = choices;
    }
    public Event(String text) {
        this.setText(text);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void addChoice(Choice choice) {
        this.choices.add(choice);
    }

    public List<String> listChoices() {
        List<String> output = new ArrayList<>();
        for(Choice choice: this.choices) {
            output.add(choice.getText());
        }
        return output;
    }

    public Event choose(int index) {
        return this.choices.get(index).getNext();
    }

}
