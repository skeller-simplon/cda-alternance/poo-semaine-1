package co.simplon.alt1.data;

public class Choice {
    private String text;
    private Event next;

    public Choice(String text, Event next) {
        this.setText(text);
        this.next = next;
    }

    public Event getNext() {
        return next;
    }

    public Choice(String text) {
        this.setText(text);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    
}
